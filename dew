#!/usr/bin/env perl
# vim: ts=3 sts=3 sw=3 et ai :
# dew - A structured data viewer
use v5.24;
use warnings;
use experimental qw< signatures >;
no warnings qw< experimental::signatures >;
use autodie;
use Pod::Usage qw< pod2usage >;
use Getopt::Long qw< GetOptionsFromArray :config gnu_getopt >;
use English qw< -no_match_vars >;
my $VERSION = '0.1';

MyViewer->new(
   get_options(
      [
         'debug!',
         'get_stderr|get-stderr!',
         'namer|name-field|n=s',
         'show_stderr|show-stderr!',
         {
            optnames => 'history|H=s',
            environment => 'DEW_HISTORY',
         },
      ],
      [@ARGV]
   )->%*
)->run;

sub get_options ($specs, $ARGV) {
   my (%cmdline, %environment, %default);
   my @cmdline_options = qw< help! man! usage! version! >;
   for my $spec ($specs->@*) {
      my ($optnames, $default, $env_var) =
        ref $spec
        ? $spec->@{qw< optnames default environment >}
        : ($spec, undef, undef);
      push @cmdline_options, $optnames;
      (my $name = $optnames) =~ s{[^\w-].*}{}mxs;
      $default{$name}     = $default       if defined $default;
      $environment{$name} = $ENV{$env_var}
         if defined $env_var && defined $ENV{$env_var};
   } ## end for my $spec ($specs->@*)

   GetOptionsFromArray($ARGV, \%cmdline, @cmdline_options)
     or pod2usage(-verbose => 99, -sections => 'USAGE');

   pod2usage(message => "$0 $VERSION", -verbose => 99, -sections => ' ')
     if $cmdline{version};
   pod2usage(-verbose => 99, -sections => 'USAGE') if $cmdline{usage};
   pod2usage(-verbose => 99, -sections => 'USAGE|EXAMPLES|OPTIONS')
     if $cmdline{help};
   pod2usage(-verbose => 2) if $cmdline{man};

   return {%default, %environment, %cmdline, __residual_args => $ARGV};
} ## end sub get_options


package MyViewer;
use v5.24;
use Curses::UI;
use Curses qw< KEY_ENTER KEY_UP KEY_DOWN KEY_HOME KEY_PPAGE KEY_NPAGE
               KEY_SF KEY_SR >;
use JSON::PP qw< decode_json >;
use YAML::Dump 'Dump';
use List::Util 'reduce';
use Capture::Tiny qw< capture capture_stdout >;
use experimental 'signatures';
no warnings 'experimental::signatures';

sub cui ($self) { return $self->{cui} }
sub new ($package, %args) { return bless(\%args, $package)->init }
sub run ($self) { $self->cui->mainloop }

sub init ($self) {
   my $cui = $self->{cui} = Curses::UI->new(
      -clear_on_exit => 1,
      -mouse_support => 0,
      -debug         => $self->{debug},
   );
   $cui->set_binding(sub { exit 0 }, "\cC", "\cQ", "\cX");

   # Order matters for overlapping windows a bit
   $self->init_menu->init_selectors_win
      ->init_command_entry->init_data_viewer;

   if (my @command = $self->{__residual_args}->@*) {
      $self->command_entry->text(join ' ', @command);
      $self->run_command;
   }
   else {
      $self->set_view("Run a command to get some data...");
      $self->focus_entry;
   }

   $self->load_history;

   return $self;
} ## end sub init ($self)

sub init_menu ($self) {
   my $cui = $self->cui;

   my $menu_data = [ { -label => 'Name path (^W)' } ];
   my $menu = $cui->add(
      menu => 'Menubar',
      -menu => $menu_data,
      -fg => 'blue',
      -bg => 'white',
   );

   $cui->set_binding(sub { $self->new_name_path }, "\cW");
   return $self;
}

sub init_selectors_win ($self) {
   my $swin = $self->cui->add(
      selectors_win => 'Window',
      -height       => 1,
      -y            => 3,
      -border       => 1,
      -userdata     => {},
   );
   $swin->set_binding(sub { exit(0) }, 'q');
   $swin->set_binding(sub { $self->viewer_pageshift('up') }, KEY_PPAGE);
   $swin->set_binding(sub { $self->viewer_pageshift('dw') }, KEY_NPAGE);
   $swin->set_binding(sub { $self->viewer_pageshift('line-up') }, KEY_SR);
   $swin->set_binding(sub { $self->viewer_pageshift('line-dw') }, KEY_SF);
   return $self;
} ## end sub init_selectors_win ($self)

sub init_command_entry ($self) {
   my $entry = $self->cui->add(
      command_win => 'Window',
      -height     => 1,
      -border     => 1,
      -y          => 1,
      -onfocus    => sub { $self->clean_entry },
   )->add(entry => 'TextEntry', -width => -1, -userdata => {});
   $entry->set_binding(sub { $self->focus_selectors }, "\x1b");
   $entry->set_binding(sub { $self->run_command },     KEY_ENTER);
   $entry->set_binding(sub { $self->history(-1) },     KEY_DOWN);
   $entry->set_binding(sub { $self->history(1) },      KEY_UP);
   $entry->set_binding(sub { $self->viewer_pageshift('up') }, KEY_PPAGE);
   $entry->set_binding(sub { $self->viewer_pageshift('down') }, KEY_NPAGE);
   $entry->set_binding(sub { $self->viewer_pageshift('line-up') }, KEY_SR);
   $entry->set_binding(sub { $self->viewer_pageshift('line-dw') }, KEY_SF);
   return $self;
} ## end sub init_command_entry ($self)

sub init_data_viewer ($self) {
   my $viewer = $self->cui->add(data_win => 'Window', -y => 6)->add(
      viewer        => 'TextViewer',
      -text         => 'XXX',
      -showoverflow => 1
   );
   $viewer->set_binding(sub { $self->focus_selectors }, KEY_ENTER, "\x1b");
   $viewer->set_binding(sub { $self->focus_entry }, ":");
   return $self;
} ## end sub init_data_viewer ($self)

sub viewer_pageshift ($self, $key) {
   my $viewer = $self->viewer;
   $key eq 'up' ? $viewer->cursor_pageup
   : $key eq 'dw' ? $viewer->cursor_pagedown
   : $key eq 'line-up' ? $viewer->cursor_up
   : $key eq 'line-dw' ? $viewer->cursor_down
   : ();
   $viewer->draw;
}

sub history ($self, $delta) {
   my $entry = $self->command_entry;
   my $ud = $entry->userdata;
   my $position = $ud->{position} + $delta;
   return if $position < 0 || $position > $ud->{provisional}->$#*;
   $ud->{provisional}->[$ud->{position}] = $entry->get;
   $entry->text($ud->{provisional}->[$ud->{position} = $position]);
   return $self;
} ## end sub history

sub clean_entry ($self) {
   my $entry = $self->command_entry;
   my $ud = $entry->userdata;
   $entry->text('');
   $ud->{provisional} = ['', ($ud->{history} //= [])->@*];
   $ud->{position} = 0;
   return $self;
} ## end sub clean_entry ($self)

sub add_history ($self, $command) {
   my $history = $self->command_entry->userdata->{history} //= [];
   unshift $history->@*, $command;
   $self->save_history;
   return $self;
}

sub save_history ($self, $filename = undef) {
   defined($filename //= $self->{history}) or return;
   open my $fh, '>:encoding(UTF-8)', "$filename.tmp" or return;
   my $current;
   say {$fh} join "\n", grep {
      (my $previous, $current) = ($current, $_);
      ! defined $previous || $previous ne $current;
   } $self->command_entry->userdata->{history}->@*;
   close $fh;
   rename "$filename.tmp", $filename;
   return $self;
}

sub load_history ($self, $filename = undef) {
   defined($filename //= $self->{history}) or return;
   open my $fh, '<:encoding(UTF-8)', $filename or return;
   chomp(my @lines = <$fh>);
   close $fh;
   $self->command_entry->userdata->{history} = \@lines;
   return $self;
}

sub run_command ($self) {
   my $entry = $self->command_entry;

   my $command = $entry->get;
   exit 0 if $command =~ m{\A q (uit)? \z}imxs;

   $self->add_history($command);

   my $ud = $entry->userdata;
   $ud->{provisional} = '';

   my $cui = $self->cui;
   $cui->leave_curses;

   my ($stdout, $stderr, $exit);
   my $runner = sub { system {'/bin/sh'} '/bin/sh', '-c', $command };
   if ($self->{get_stderr}) {
      ($stdout, $stderr, $exit) = capture { $runner->() };
   }
   else {
      ($stdout, $exit) = capture_stdout { $runner->() };
   }
   $cui->reset_curses;

   if ($exit) {
      $cui->dialog($stderr //= "An error occurred, exit code $exit");
      return;
   }
   if ($self->{show_stderr} && defined $stderr) {
      $cui->dialog($stderr);
   }

   if (defined(my $data = eval { decode_json($stdout) })) {
      $self->show($data);
   }

   return $self;
} ## end sub run_command ($self)

sub run_or_dialog ($self, $cb) {
   eval { $cb->(); 1 } and return;
   $self->cui->dialog($@);
}

sub _get ($self, @chain) {
   reduce { $a->getobj($b) } $self->cui, @chain;
}

sub command_entry ($self) { $self->_get(qw< command_win entry >) }
sub selectors_win ($self) { $self->_get(qw< selectors_win >) }
sub viewer ($self)        { $self->_get(qw< data_win viewer >) }

sub set_view ($self, $data, $home = !!0) {
   my $viewer = $self->viewer;
   $viewer->text($data);
   $viewer->process_bindings(KEY_HOME) if $home;
   $viewer->draw;
   return $self;
} ## end sub set_view

sub focus_entry ($self)     { $self->command_entry->focus }
sub focus_selectors ($self) { $self->selectors_win->focus }
sub focus_viewer ($self)    { $self->viewer->focus }

sub names_for ($self, $aref) {
   my $namer = $self->{namer} // '';
   my @namer = split m{\.}, $namer;
   my $name_for = sub ($item) {
      return unless @namer;
      for my $step (@namer) {
         defined($item = $item->{$step}) or last;
      }
      return $item;
   };
   return +{
      map {
         $_ => $name_for->($aref->[$_]) // ($_ + 1)
      } 0 .. $aref->$#*
   };
}

sub add_selector ($self, $keys, $labels = {}) {
   my $selwin = $self->selectors_win;
   my $ud     = $selwin->userdata;
   my $sels   = $ud->{selectors} //= [];

   # calculate offset
   my $offset = 0;
   $offset += $selwin->getobj($_)->width + 2 for $sels->@*;

   push $sels->@*, my $id = 'selector-' . $sels->@*;
   my $update_cb = sub {
      $self->show_data_slice;
      $self->cui->draw;
   };
   my $obj = $selwin->add(
      $id          => 'Popupmenu',
      -values      => $keys,
      -labels      => $labels,
      -wraparound  => 1,
      -selected    => 0,
      -x           => $offset,
      -onchange    => $update_cb,
      -onselchange => $update_cb,
      -onfocus     => $update_cb,
      -onblur      => $update_cb,
   );
   $obj->set_binding(sub { $self->focus_entry },  ":");
   $obj->set_binding(sub { $self->focus_viewer }, ".");

   return $self;
} ## end sub add_selector

sub show_data_slice ($self) {
   return if $self->{setting_up};
   my $data            = $self->{data} //= {};
   my $selwin          = $self->selectors_win;
   my $ud              = $selwin->userdata;
   my @selectors_names = ($ud->{selectors} // [])->@*;
   my $key;
   for my $name (@selectors_names) {
      my $selector = $selwin->getobj($name);
      $key = $selector->get;
      $data =
          ref $data eq 'ARRAY' ? $data->[$key]
        : $key eq '*'          ? $data
        :                        $data->{$key};
   } ## end for my $name (@selectors_names)

   my $reset_to_home = ($ud->{last_key} // '') ne $key;
   $self->set_view(Dump($data), $reset_to_home);
   $ud->{last_key} = $key;
   return $self;
} ## end sub show_data_slice ($self)

sub show ($self, $data = undef) {
   return $self unless defined($data //= $self->{data});

   $self->set_view('ok, showing...');
   $self->{setting_up} = 1;

   my $selwin = $self->selectors_win;
   my $ud = $selwin->userdata;

   my $selectors = $ud->{selectors} // [];
   $ud->{selectors} = [];
   $selwin->delete($_) for $selectors->@*;

   my %forkeys;
   if (ref $data eq 'ARRAY') {
      %forkeys = (%forkeys, $_->%*) for $data->@*;
      $self->add_selector([0 .. $data->$#*], $self->names_for($data));
   }
   else {
      %forkeys = $data->%*;
   }

   my @keys = ((sort { $a cmp $b } keys %forkeys), '*');
   $self->add_selector(\@keys);

   $self->{setting_up} = 0;
   $self->{data}       = $data;
   $self->show_data_slice;

   $self->focus_selectors;
   return $self;
} ## end sub show

sub new_name_path ($self) {
   $self->{namer} = $self->cui->question('Name path?');
   $self->focus_viewer; # workaround to re-focus on selectors in ->show
   $self->show;
}

1;

__END__

=pod

=encoding utf-8

=head1 NAME

dew - A structured data viewer

=head1 VERSION

The version can be retrieved with option C<--version>:

   $ dew --version

=head1 USAGE

   dew [--help] [--man] [--usage] [--version]

   dew

=head1 EXAMPLES

   # Some examples will help...

=head1 DESCRIPTION

=head1 OPTIONS

=over

=item B<--help>

print out some help and exit.

=item B<--man>

show the manual page for dew.

=item B<--usage>

show usage instructions.

=item B<--version>

show version.

=back

=head1 CONFIGURATION

=head1 DIAGNOSTICS

=head1 DEPENDENCIES

=head1 BUGS AND LIMITATIONS

Please report any bugs or feature requests through the repository at
L<>.

=head1 AUTHOR

Flavio Poletti

=head1 LICENSE AND COPYRIGHT

Copyright 2022 by Flavio Poletti (flavio@polettix.it).

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

or look for file C<LICENSE> in this project's root directory.

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=cut
